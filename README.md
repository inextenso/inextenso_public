# InExtenso_Public

Ce dépôt est l'espace de partage public du projet [ANR InExtenso](https://project.inria.fr/anrinextenso/).

Il est en cours de construction et contiendra, à terme, une bibliographie, le code et les données créés dans le cadre du projet.
